from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class ToDoItem(models.Model):
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    date_created = models.DateTimeField('date created')
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


class MyEvents(models.Model):
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    event_date = models.DateField('event date')
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
